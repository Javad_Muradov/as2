<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmst" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
    <title>PROFILE</title>
<style>
    table, th, td {
        border: 1px solid black;;
    }

    th, td {
        padding: 15px;
    }

    table{
        width: 400px;
        border-spacing: 5px;
    }

    #logout{
        width: 80px;
        height: 40px;

    }
    table{
        float: left;
    }
    button {
        width: 100%;
        height: 48px;
        float: right;
    }


</style>
</head>
<body>
<p>Sorry Anar mellim :( I couldnot finish the enrollment part of the assignment ( I  have Cyber security exam  ) Wish me luck :)</p>

<div class="table" id="pInfo">

    <table >
        <tr>
            <th colspan="2">Personal Information(Fetched from DB)</th>

        </tr>
        <tr>
            <td><b>First Name</b></td>
            <td><c:out value="${sessionScope.FName}"/></td>
        </tr>
        <tr>
            <td><b>Last Name</b></td>
            <td><c:out value="${sessionScope.LName}"/></td>
        </tr>
        <tr>
            <td><b>Faculty</b></td>
            <td><c:out value="${sessionScope.fclty}"/></td>
        </tr>
        <tr>
            <td><b>Gender</b></td>
            <td><c:out value="${sessionScope.Cntry}"/></td>
        </tr>
        <tr>
            <td><b>Graduation</b></td>
            <td><c:out value="${sessionScope.grdt}"/></td>
        </tr>
        <tr>
            <td><b>Country</b></td>
            <td><c:out value="${sessionScope.gndr}"/></td>
        </tr>

    </table>
</div>
<div class="table" id="courses" >
    <table style="margin-left:30px " >
        <tr>
            <th colspan="6">Available Courses(Fetched from DB)</th>
        </tr>
        <tr>
            <td><b>Course</b></td>
            <td><b>Instructor_Name</b></td>
            <td><b>Surname</b></td>
            <td><b>Credit</b></td>
            <td><b>Status</b></td>

        </tr>
        <c:forEach var="subject" items="${sessionScope.crss}">
            <tr>
                <td><c:out value="${subject.course_Name}"/></td>
                <td><c:out value="${subject.name}"/></td>
                <td><c:out value="${subject.surname}"/></td>
                <td><c:out value="${subject.credit}"/></td>

                <td>
                    <form action="#" method="post">
                        <button>Enroll</button>
                    </form>
                </td>
            </tr>
        </c:forEach>

    </table>
</div>
<div>
    <button id="logout"><a href="/HomePage/auth?action=logout">LOGOUT</a></button>
</div>
</body>
</html>