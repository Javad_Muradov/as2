package controller;

import Model.Student;
import utils.DBUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class RegistrationControllerServlet extends HttpServlet {
    public static final String New_User="is_created";
    public static final String User_id="ID";
    public static final String User_email="Email";
    public static final String FIRST_NAME = "FName";
    public static final String Last_NAME = "LName";
    public static final String Country = "Cntry";
    public static final String Faculty = "fclty";
    public static final String Gender = "gndr";
    public static final String Graduation = "grdt";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String buttonType = req.getParameter("btnname");


            if(buttonType!=null && buttonType.equals("Finish")){
                String faculty = req.getParameter("faculty");
                String grad_year = req.getParameter("grad_year");
                String country = req.getParameter("country");
                String gender = req.getParameter("gender");
                String email =(String) req.getSession().getAttribute(RegistrationControllerServlet.User_email);
                DBUtils.UpdateUser(faculty,grad_year,country,gender,email);
                req.getSession().removeAttribute(RegistrationControllerServlet.New_User);


                resp.sendRedirect(req.getContextPath()+"/");
                return;


            }

        String email = req.getParameter("N_useremail");
        String userName = req.getParameter("N_username");
        String userLastName = req.getParameter("N_usersurname");
        String userpassword = req.getParameter("N_userpassword");



        HttpSession session;

        int result= DBUtils.addUser(email,userName,userLastName,userpassword);
        if(result==1){
            System.out.println("WELCOME NEW USER");
            System.out.println("YOu will be forwarded to Personal DATA PAGE, Pls fill the required fields");
            session=req.getSession();
            session.setAttribute(RegistrationControllerServlet.New_User, true);
            session.setAttribute(RegistrationControllerServlet.User_email,email);




            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(req,resp);


        }else {
            resp.sendRedirect("/HomePage");
            System.out.println("User cannot be created---> Pls enter again");
            getServletConfig().getServletContext().getRequestDispatcher("/index.jsp");
        }




    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }
}
