package controller;

import Model.Student;
import utils.DBUtils;
import utils.Hash;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthControllerServlet extends HttpServlet {
    private static final String AUTHORIZE_FIELD="is_authorize";
    private static final String CURRENT_USER="current_username";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("REQUEST FROM SERVER REACHED THE SERVLET");
        HttpSession session = req.getSession();

        if(session.getAttribute(AuthControllerServlet.AUTHORIZE_FIELD) != null) {
            if (req.getParameter("action") != null && req.getParameter("action").equals("logout")) {

                System.out.println("Successfully LOGGED OUT");
                req.getSession().removeAttribute(RegistrationControllerServlet.New_User);
                session.removeAttribute(AuthControllerServlet.AUTHORIZE_FIELD);
                session.removeAttribute(AuthControllerServlet.CURRENT_USER);

            }
        }
        getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // take info from the Form in auth.jsp
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        password= Hash.hash(password);

        // Length of username should be more than 2 and password should be equal to 6
        if(email!=null && password!=null){
            try {
               ResultSet resultSet= DBUtils.query("Select count(1) as user_exist from users where email=? and userpassword=? ",
                       new String[] {email,password});
                if(resultSet!=null && resultSet.next() && resultSet.getInt("user_exist")>0){

                    HttpSession session=req.getSession();
                    session.setAttribute(AuthControllerServlet.AUTHORIZE_FIELD, true);
                    session.setAttribute(AuthControllerServlet.CURRENT_USER, email);
                    System.out.println("User passed the GATE");
                    Student student = DBUtils.getUser(email);
                    System.out.println(student.userName);
                    req.getSession().setAttribute(RegistrationControllerServlet.User_id,student.id);
                    req.getSession().setAttribute(RegistrationControllerServlet.FIRST_NAME,student.userName);
                    req.getSession().setAttribute(RegistrationControllerServlet.Last_NAME,student.userLastName);
                    req.getSession().setAttribute(RegistrationControllerServlet.Country,student.country);
                    req.getSession().setAttribute(RegistrationControllerServlet.Gender,student.gender);
                    req.getSession().setAttribute(RegistrationControllerServlet.Graduation,student.grad_year);
                    req.getSession().setAttribute(RegistrationControllerServlet.Faculty,student.faculty);
                    resp.sendRedirect(req.getContextPath()+"/profile");

                } else{
                    System.out.println("There is no user in the system!");
                    req.setAttribute("error_message",new String[]{"There is no user in the system!"});
                    resp.sendRedirect("/HomePage");
                }
            } catch (SQLException throwables) {
                req.setAttribute("error_message",new String[]{"ERROR DURING CONNECTING THE DATABASE"});

                throwables.printStackTrace();

            }
        } else {
            System.out.println("DATA VALIDATION ERROR  ");
            req.setAttribute("error_message",new String[]{"DATA VALIDATION ERROR"});

        }
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void init() throws ServletException {
        super.init();
    }
}
