package controller;

import Model.Course;
import utils.DBUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class ProfileControllerServlet extends HttpServlet {
    public static final String COURSES="crss";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Course> courses = DBUtils.fetchCourse();
        System.out.println(courses.size());
        req.getSession().setAttribute(ProfileControllerServlet.COURSES,courses);


        resp.sendRedirect("/HomePage/profile.jsp");
        //super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //super.doPost(req, resp);
    }
}
