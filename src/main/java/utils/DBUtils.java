package utils;
import Model.Course;
import Model.Student;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

public class DBUtils {

    static DataSource dataSource;

    static  {
        System.out.println("YOU ARE IN");
        try {
            Context initContext=new InitialContext();
            Context webContext =(Context) initContext.lookup("java:/comp/env");
            dataSource=(DataSource) webContext.lookup("jdbc/user_datasource");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    private static void clsCOnnection(Connection connection) throws SQLException {

        if (connection!=null) connection.close();
    }

     public static ResultSet query (String sql, String... args) throws SQLException {
         Connection connection = null;
         PreparedStatement statement = null;
         ResultSet resultSet = null;
        try {
            connection= DBUtils.dataSource.getConnection();
            statement = connection.prepareStatement(sql);

            for(int i=0 ; i<= args.length-1;i++){
                statement.setString(i+1,args[i]);
            }
             return statement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }

         return null;
    }


    public static int addUser (String email,String userName,String userLastName,String userpassword )  {
        userpassword=Hash.hash(userpassword);
        String insert = "insert into users(email, userName, userLastName, userpassword) values (?,?,?,?)";
        Connection newConnection=null;

        int Result=0;


            try {
                newConnection = DBUtils.dataSource.getConnection();
                PreparedStatement preparedStatement = newConnection.prepareStatement(insert);
                preparedStatement.setString(1, email);
                preparedStatement.setString(2, userName);
                preparedStatement.setString(3, userLastName);
                preparedStatement.setString(4, userpassword);

                int resultSet = preparedStatement.executeUpdate();

                return 1;
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    clsCOnnection(newConnection);
                } catch (SQLException e) {
                    e.printStackTrace();
                }finally
                {
                    try {
                        clsCOnnection(newConnection);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }

        return 0;
    }
    public static int UpdateUser (String faculty,String grad_year,String country,String gender,String email){

        String update = "update users set faculty=? , grad_year=? ,country=?,gender=? where email =?";
        Connection InfoConnection=null;


            try {
                InfoConnection = DBUtils.dataSource.getConnection();
                PreparedStatement preparedStatement = InfoConnection.prepareStatement(update);
                preparedStatement.setString(1, faculty);
                preparedStatement.setString(2, grad_year);
                preparedStatement.setString(3, country);
                preparedStatement.setString(4, gender);
                preparedStatement.setString(5, email);

                int resultSet = preparedStatement.executeUpdate();

                return 1;
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    clsCOnnection(InfoConnection);

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        return 0;


    }

    public static Student getUser(String email){

            String query = "select * from users where email = ?";
            Connection connection = null;
            Student student = null;

            try
            {
                connection = DBUtils.dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.setString(1, email);
                ResultSet resultSet = preparedStatement.executeQuery();

                student = parseStudent(resultSet);
                resultSet.close();
            } catch (SQLException e)
            {
                e.printStackTrace();
            } finally
            {
                try {
                    clsCOnnection(connection);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            return student;

    }

    private static Student parseStudent(ResultSet resultSet)
    {
        try
        {
            if (resultSet.next())
            {
                int userId = resultSet.getInt("user_id");
                String firstName = resultSet.getString("userName");
                String lastName = resultSet.getString("userLastName");
                String country = resultSet.getString("country");
                String gender = resultSet.getString("gender");
                String faculty = resultSet.getString("faculty");
                String grad_year = resultSet.getString("grad_year");

                return new Student(userId,firstName,lastName,country,gender,faculty,grad_year);
            }
        } catch (SQLException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public static ArrayList<Course> fetchCourse(){

        String getCourse="select * from courses";
        Connection connection=null;
        ResultSet resultSet=null;
        ArrayList<Course> courses=new ArrayList<>();
        try {
            connection=DBUtils.dataSource.getConnection();
            Statement statement=connection.createStatement();
            resultSet=statement.executeQuery(getCourse);
            while (resultSet.next())
            {
                int ID = resultSet.getInt("course_id");
                String crsName = resultSet.getString("courseName");
                String name = resultSet.getString("instructor_name");
                String lastname = resultSet.getString("instructor_surname");
                int credit = resultSet.getInt("cretid");

                courses.add(new Course(ID, crsName,name,lastname,credit));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally
        {
            try {
                clsCOnnection(connection);
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return courses;
    }

}
