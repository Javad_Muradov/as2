package utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash {
    static MessageDigest messageDigest;

    static {
        try {
            messageDigest = MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String hash(String string) {
        byte[] encryptedPassword = messageDigest.digest(string.getBytes(StandardCharsets.UTF_8));
        StringBuffer hashedString = new StringBuffer();

        for (int i = 0; i < encryptedPassword.length; i++) // Converting bytes array to string
            hashedString.append(Integer.toString((encryptedPassword[i] & 0xff) + 0x100, 16).substring(1));

        return hashedString.toString();
    }
}