package Model;

public class Student
{
    public int id;
    public String userName;
    public String userLastName ;
    //public String  email ;
    public String gender ;
    public  String  country ;
    public String faculty ;
    public String grad_year ;

    public Student(int id, String userName, String userLastName,  String gender, String country, String faculty, String grad_year) {
        this.id = id;
        this.userName = userName;
        this.userLastName = userLastName;
        this.gender = gender;
        this.country = country;
        this.faculty = faculty;
        this.grad_year = grad_year;
    }
}
