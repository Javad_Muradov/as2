package Model;

public class Course {
    public int course_ID;
    public String course_Name;
    public String name;
    public String surname;
    public int credit;

    public Course(int course_ID, String course_Name,String name,String surname,int credit) {
        this.course_ID = course_ID;
        this.course_Name = course_Name;
        this.name=name;
        this.surname=surname;
        this.credit=credit;


    }

    public String getCourse_Name() {
        return course_Name;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getCredit() {
        return credit;
    }
}
